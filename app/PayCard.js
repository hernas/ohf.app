import React from 'react'
import Card from 'react-credit-card';

require('react-credit-card/source/card.css');
require('react-credit-card/source/card-types.css');

require('font-awesome-animation');

require('./styles/custom-card-faces.less');
require('./styles/back-card-face.less');
/*
//checks if react-credit-card supports that face
function isFaceSupportedNatively(faceType) {
    return ['visa', 'amex', 'mastercard', 'discover'].indexOf(faceType) !== -1;
}*/

class PayCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            focus: 'number'
        };
    }

    turnOver() {
        this.setState({timeToLive: 15});
        clearInterval(this.timer);
        //don't ask me about react-credit-card API
        var newValue = this.state.focus === 'cvc'? 'number' : 'cvc';
        //back side
        if (newValue == 'cvc') {
            this.timer = setInterval(() => {
                console.log(this.state.timeToLive);
                this.setState({timeToLive: this.state.timeToLive-1});

                if (this.state.timeToLive < 1) {
                    clearInterval(this.timer);
                    this.setState({focus: 'number'}); //flip
                }
            }, 1000);
        }
        this.setState({focus: newValue});
    }

    render() {
        return (
            <div className={this.props.face} onClick={this.turnOver.bind(this)}>
                <Card type={this.props.face} number={this.props.number || this.props.email} name={this.props.name} expiry={this.props.expiry} cvc={this.props.cvc} focused={this.state.focus} isEmail={!!this.props.email}>
                    <img src={this.props.qrCode} />
                    <div className="alert-wrapper"><i className="fa fa-spinner faa-spin animated"></i> <div className="alert-inner">Invalidating in <br/>{this.state.timeToLive} seconds...</div></div>
                </Card>
            </div>)
    }
}

PayCard.defaultProps = {
    number: "",
    email: "",
    name: "Teodor Patras",
    expiry: "12/20",
    cvc:"000"
};

export default PayCard;
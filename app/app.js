import React from 'react'
import Router, {Route} from 'react-router'
import Button from 'react-button'

require('bootstrap');
require('font-awesome');
$ = require('jquery');

import PayCard from './PayCard.js'
require('./styles/app.less');

const API_URL = 'http://ohf.hern.as/';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cards: [],
            selectedId: 0
        };
    }

    componentDidMount() {
        $.get(`${API_URL}/payments/`, (res) => {
            this.setState({cards: res.map(this.transformResponse)});
        });
    }

    transformResponse(response) {
        var transformed = {};

        if (response.provider === 'pp') {
            transformed.face = 'paypal';
        } else {
            transformed.face = response.card_type.toLowerCase()
        }

        transformed.qrCode = response.qr;
        transformed.name = response.data.name;
        transformed.number = response.data.number;
        transformed.email = response.data.email;

        transformed.ccv = response.data.ccv;

        return transformed;
    }

    prevCard() {
        this.setState({selectedId: (this.state.selectedId - 1) % this.state.cards.length});
    }

    nextCard() {
        this.setState({selectedId: (this.state.selectedId + 1) % this.state.cards.length});
    }

    render() {
        return(
            <div>
                <PayCard {...this.state.cards[this.state.selectedId]} />
                <div className="buttons">
                    <Button onClick={this.prevCard.bind(this)}><i className="fa fa-chevron-left"></i></Button> &nbsp;
                    <Button onClick={this.nextCard.bind(this)}><i className="fa fa-chevron-right"></i></Button>
                </div>
            </div>
        );
    }
}

React.render((
  <Router>
    <Route path="/" component={App} />
  </Router>
), document.getElementById("app"));
